#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include "ihs.hpp"

namespace py = pybind11;

py::array_t<int> ihs_wrapper(int m, int n, int d, int seed) {
	int *r = ihs(m, n, d, seed);
	const int count = m * n;

	py::capsule free_when_done(r, [](void *f) {
		int *foo = reinterpret_cast<int *>(f);
		delete[] foo;
	});

	return py::array_t<int>(count, r);
}

PYBIND11_MODULE(IHSModule, m) {
	m.doc() = "IHS Library"; // optional module docstring

	m.def("ihs", &ihs_wrapper, "IHS generator");
}